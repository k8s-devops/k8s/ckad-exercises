Create a busybox pod that runs 'i=0; while true; do echo "$i: $(date)"; i=$((i+1)); sleep 1; done'. Check its logs
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --restart Never -- bin/sh -c 'i=0; while true; do echo "$i: $(date)"; i=$((i+1)); sleep 1; done'

kubectl logs busybox -f
```

</p>
</details>

Create a busybox pod that runs 'ls /notexist'. Determine if there's an error (of course there is), see it. In the end, delete the pod
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --restart Never -- bin/sh -c 'ls /notexist'

kubectl logs busybox 

kubectl delete pod busybox
```

</p>
</details>

Create a busybox pod that runs 'notexist'. Determine if there's an error (of course there is), see it. In the end, delete the pod forcefully with a 0 grace period
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --restart Never -- bin/sh -c 'notexist'

kubectl logs busybox

kubectl describe pod busybox

kubectl get events | grep error

kubectl delete pod busybox
```

</p>
</details>

Get CPU/memory utilization for nodes (metrics-server must be running)
===

<details><summary>show</summary>
<p>

```bash
kubectl top nodes
```

</p>
</details>
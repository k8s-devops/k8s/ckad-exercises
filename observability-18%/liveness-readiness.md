Create an nginx pod with a liveness probe that just runs the command 'ls'. Save its YAML in pod.yaml. Run it, check its probe status, delete it.
===

<details><summary>show</summary>
<p>

```bash
# create the manifest file
kubectl run nginx --image=nginx --restart Never --dry-run=client -o yaml > pod.yml

# add liveness prob block

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - image: nginx
#    name: nginx
#    livenessProbe:
#      exec:
#        command:
#          - ls
#    resources: {}
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never

# create the pod
kubectl create -f pod.yml

kubectl describe pods nginx | grep --ignore-case liveness

kubectl delete -f pod.yml

```

</p>
</details>

<br/>

Modify the pod.yaml file so that liveness probe starts kicking in after 5 seconds whereas the interval between probes would be 5 seconds. Run it, check the probe, delete it.
===

<details><summary>show</summary>
<p>

```bash
# add the two new options

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - image: nginx
#    name: nginx
#    livenessProbe:
#      exec:
#        command:
#          - ls
#      initialDelaySeconds: 5
#      periodSeconds: 5
#    resources: {}
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never

# create the pod
kubectl create -f pod.yml

kubectl describe pods nginx | grep --ignore-case liveness

kubectl delete -f pod.yml

```

</p>
</details>

<br/>

Create an nginx pod (that includes port 80) with an HTTP readinessProbe on path '/' on port 80. Again, run it, check the readinessProbe, delete it.
===

<details><summary>show</summary>
<p>

```bash
# create the pod manifest yaml file
kubectl run nginx --image=nginx --port 80 --restart Never --dry-run=client -o yaml > pod.yml

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - image: nginx
#    name: nginx
#    readinessProbe:
#      httpGet:
#        path: /
#        port: 80
#    ports:
#    - containerPort: 80
#    resources: {}
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never
#status: {}

# create the pod
kubectl create -f pod.yml --save-config

kubectl describe pod nginx  | grep Readiness

kubectl delete -f pod.yml

```

</p>
</details>

<br/>
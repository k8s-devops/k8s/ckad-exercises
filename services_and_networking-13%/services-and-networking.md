Create a pod with image nginx called nginx and expose its port 80
===

<details><summary>show</summary>
<p>

```bash
kubectl run nginx --image=nginx --restart Never --port 80 --expose 
```

</p>
</details>

<br/>

Confirm that ClusterIP has been created. Also check endpoints
===

<details><summary>show</summary>
<p>

```bash
kubectl get service ngix
kubectl get endpoint
```

</p>
</details>

<br/>

Convert the ClusterIP to NodePort for the same service and find the NodePort port. Hit service using Node's IP. Delete the service and the pod at the end.
===

<details><summary>show</summary>
<p>

```bash
kubectl edit svc nginx
# change ClusterIP by NodePort
Kubectl get svc -o wide
# grap the node IP and the NodePort port
# port in range (30000-32767)
kubectl delete svc nginx
kubectl delete pod nginx
```

</p>
</details>

<br/>

Create a deployment called foo using image 'dgkanatsios/simpleapp' (a simple server that returns hostname) and 3 replicas. Label it as 'app=foo'. Declare that containers in this pod will accept traffic on port 8080 (do NOT create a service yet)
===

<details><summary>show</summary>
<p>

```bash
kubectl create deployment foo --image=dgkanatsios/simpleapp --port 8080 --replicas 3
```

</p>
</details>

<br/>

Get the pod IPs. Create a temp busybox pod and trying hitting them on port 8080
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods -l app=foo -o wide

kubect run busybox --imge=busybox -it --rm -- sh
wget -O- POD_IP:8080
exit
```

</p>
</details>

<br/>

Create a service that exposes the deployment on port 6262. Verify its existence, check the endpoints
===

<details><summary>show</summary>
<p>

```bash
kubectl expose deployment foo --port 6262 --target-port 8080
kubectl get svc foo
kubectl get ep #endpoints
```

</p>
</details>

<br/>

Create a temp busybox pod and connect via wget to foo service. Verify that each time there's a different hostname returned. Delete deployment and services to cleanup the cluster
===

<details><summary>show</summary>
<p>

```bash
# get the foo service ClusterIP 
kubectl get svc foo
# run a busybox pod
kubectl run busybox --image=busybox --restart Never -it --rm -- sh
# execute wget 
wget -O- foo:6262
wget -O- SERVICE_CLUSTER_IP:6262
exit
# delete all resources
kubectl delete svc foo
kubectl delete deployment foo
```

</p>
</details>

<br/>

Create an nginx deployment of 2 replicas, expose it via a ClusterIP service on port 80. Create a NetworkPolicy so that only pods with labels 'access: granted' can access the deployment and apply it
===

<details><summary>show</summary>
<p>

```bash
# prerequisite
install & enable NetworkPolicy
https://kubernetes.io/docs/tasks/administer-cluster/network-policy-provider/cilium-network-policy/
# create the deployment
kubectl create deployment nginx --image=nginx --replicas 2 
# expose the service
kubectl expose deployment nginx --port 80 --target-port 80
# check the netpol.yml manifest file and apply the file
kubectl create -f netpol.yml --save-config --record
# check the access from pods with 'access: granted' label and others without it
# does't work
kubectl run busybox --image=busybox --rm -it --restart Never -- wget -O- nginx:80 --timeout 2
# work
kubectl run busybox --image=busybox --rm -it --restart Never --labels access=granted -- wget -O- nginx:80 --timeout 2
```

</p>
</details>

<br/>

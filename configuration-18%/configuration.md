Create a configmap named 'mycm' with values foo=lala,foo2=lolo
===

<details><summary>show</summary>
<p>

```bash
kubectl create cm mycm --from-literal=foo=lala --from-literal=foo2=lolo
```

</p>
</details>

<br/>

Display its values
===

<details><summary>show</summary>
<p>

```bash
kubectl describe cm mycm

# OR 

kubectl get cm mycm -o jsonpath='{.data}{"\n"}'
```

</p>
</details>

<br/>


Create and display a configmap from a file
===

<details><summary>show</summary>
<p>

```bash
kubectl create configmap my-config --from-file=data
kubectl get cm my-config -o jsonpath='{.data}{"\n"}'
```

</p>
</details>

<br/>

Create and display a configmap from a .env file
===

<details><summary>show</summary>
<p>

```bash
kubectl create configmap my-config --from-env-file=data.env
kubectl get cm my-config -o jsonpath='{.data}{"\n"}'
kubectl describe cm my-config
```

</p>
</details>

<br/>

Create and display a configmap from a file, giving the key 'special'
===

<details><summary>show</summary>
<p>

```bash
kubectl create configmap my-config --from-file=special=data
kubectl get cm my-config -o jsonpath='{.data}{"\n"}'
kubectl describe cm my-config
```

</p>
</details>

<br/>

Create a configMap called 'options' with the value var5=val5. Create a new nginx pod that loads the value from variable 'var5' in an env variable called 'option'
===

<details><summary>show</summary>
<p>

```bash
# create the 'options' config map
kubectl create cm options --from-literal=var5=val5
# create a new pod
kubectl run nginx --image=nginx --restart Never --dry-run -o yaml > nginx.yml
# edit the nginx.yml file and add the env block to match the desired result

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - name: nginx
#    image: nginx
#    env:
#    - name: option
#      valueFrom:
#        configMapKeyRef:
#          name: options
#          key: var5
#  restartPolicy: Never

# Check result
kubectl exec -it nginx -- env | grep option

```

</p>
</details>

<br/>

Create a configMap 'mycm' with values 'var6=val6', 'var7=val7'. Load this configMap as env variables into a new nginx pod
===

<details><summary>show</summary>
<p>

```bash
# create cm
kubectl create cm mycm --from-literal=var6=val6 --from-literal=var7=val7
# create a new pod
kubectl run nginx --image=nginx --restart Never --dry-run=client -o yaml > nginx.yml
# edit the nginx.yml file and add the env block to match the desired result

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - name: nginx
#    image: nginx
#    envFrom:
#    - configMapRef:
#        name: mycm
#  restartPolicy: Never

# check result
kubectl exec -it nginx -- env | grep var
```

</p>
</details>

<br/>

Create a configMap 'mycmvolume' with values 'var8=val8', 'var9=val9'. Load this as a volume inside an nginx pod on path '/etc/my-config'. Create the pod and 'ls' into the '/etc/lala' directory.
===

<details><summary>show</summary>
<p>

```bash
# create cm
kubectl create cm mycmvolume --from-literal=var8=val8 --from-literal=var9=val9
# create a new pod
kubectl run nginx --image=nginx --restart Never --dry-run=client -o yaml > nginx.yml
# edit the nginx.yml file and add volumes and volumeMounts blocks
#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - image: nginx
#    name: nginx
#    volumeMounts:
#    - name: my-cm-volume
#      mountPath: /etc/my-config
#  volumes:
#  - name: my-cm-volume
#    configMap:
#      name: mycmvolume
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never
# check result
kubectl exec -it nginx -- bin/bash
ls /etc/my-config
```

</p>
</details>

<br/>
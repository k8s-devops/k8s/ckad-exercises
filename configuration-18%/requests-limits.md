Create an nginx pod with requests cpu=100m,memory=256Mi and limits cpu=200m,memory=512Mi
===

<details><summary>show</summary>
<p>

```bash
kubectl run nginx --image=nginx --requests='cpu=200m,memory=512Mi' --limits='cpu=100m,memory=256Mi' --dry-run=client -o yaml
```

</p>
</details>

<br/>

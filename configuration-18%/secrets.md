Create a secret called mysecret with the values password=mypasss
===

<details><summary>show</summary>
<p>

```bash
kubectl create secret generic my-secret --from-literal=password=mypas
```

</p>
</details>

<br/>

Create a secret called mysecret2 that gets key/value from a file
===

<details><summary>show</summary>
<p>

```bash
kubectl create secret generic mysecret2 --from-file=secret
```

</p>
</details>

<br/>

Get the value of mysecret2
===

<details><summary>show</summary>
<p>

```bash
# method 1
# grab the secret from the yaml file
kubectl get secret mysecret2 -o yaml
# decode it
echo "MWYyZDFlMmU2N2Rm" | base64 -d

# method 2
kubectl get secrets mysecret2 -o jsonpath='{.data.secret}{"\n"}' | base64 -d
```

</p>
</details>

<br/>

Create an nginx pod that mounts the secret mysecret in a volume on path /etc/foo
===

<details><summary>show</summary>
<p>

```bash
# create the secret mysecret
kubectl create secret generic mysecret --from-file=secret

# create the pod
kubectl run nginx --image=nginx --restart Never --dry-run=client -o yaml > nginx.yml

# mount the volume to the nginx pod
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - image: nginx
#    name: nginx
#    volumeMounts:
#      - name: secret-volume
#        mountPath: /etc/foo
#        readOnly: true
#    resources: {}
#  volumes:
#  - name: secret-volume
#    secret:
#      secretName: mysecret
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never

```

</p>
</details>

<br/>

Delete the pod you just created and mount the variable 'pass' from secret mysecret onto a new nginx pod in env variable called 'PASSWORD'
===

<details><summary>show</summary>
<p>

```bash
# create the secret mysecret
kubectl create secret generic mysecret --from-file=secret

# mount variables to the env variables

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  containers:
#  - image: nginx
#    name: nginx
#    env:
#      - name: PASSWORD
#        valueFrom:
#          secretKeyRef:
#            name: mysecret
#            key: secret # file name
#    resources: {}
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never

# create the pod
kubectl create -f nginx.yml --save-config

# check env from the container
kubectl exec -it nginx -- env

```

</p>
</details>

<br/>
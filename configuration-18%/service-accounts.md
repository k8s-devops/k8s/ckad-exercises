See all the service accounts of the cluster in all namespaces
===

<details><summary>show</summary>
<p>

```bash
kubectl get sa --all-namespaces

# OR

kubectl get sa -A

```

</p>
</details>

<br/>

Create a new serviceaccount called 'myuser'
===

<details><summary>show</summary>
<p>

```bash
kubectl create serviceaccount myuser

```

</p>
</details>

<br/>

Create an nginx pod that uses 'myuser' as a service account
===

<details><summary>show</summary>
<p>

```bash
# create pod manifest yaml
kubectl run nginx --image=nginx --restart Never --serviceaccount=myuser --dry-run=client -o yaml > nginx.yml

# create the pod resource
kubectl create -f nginx.yml --save-config
```

</p>
</details>

<br/>

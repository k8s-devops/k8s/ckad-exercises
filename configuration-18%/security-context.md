Create the YAML for an nginx pod that runs with the user ID 101. No need to create the pod
===

<details><summary>show</summary>
<p>

```bash
# create a new pod
kubectl run nginx --image=nginx --restart Never --dry-run=client -o yaml > nginx.yml
# add the user ID 101
#apiVersion: v1
#kind: Pod
#metadata:
#  creationTimestamp: null
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  securityContext:
#    runAsUser: 101
#  containers:
#  - image: nginx
#    name: nginx
#    resources: {}
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never
#status: {}

```

</p>
</details>

<br/>

Create the YAML for an nginx pod that has the capabilities "NET_ADMIN", "SYS_TIME" added on its single container
===

<details><summary>show</summary>
<p>

```bash
# create a new pod
kubectl run nginx --image=nginx --restart Never --dry-run=client -o yaml > nginx.yml
# add the user ID 101
#apiVersion: v1
#kind: Pod
#metadata:
#  creationTimestamp: null
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  securityContext:
#    capabilities:
#      add: ["NET_ADMIN", "SYS_TIME"]
#  containers:
#  - image: nginx
#    name: nginx
#    resources: {}
#  dnsPolicy: ClusterFirst
#  restartPolicy: Never
#status: {}

```

</p>
</details>

<br/>

Create a job named pi with image perl that runs the command with arguments "perl -Mbignum=bpi -wle 'print bpi(2000)'"
===

<details><summary>show</summary>
<p>

```bash
kubectl create job pi --image=perl -- bin/sh -c "perl -Mbignum=bpi -wle 'print bpi(2000)'"
```

</p>
</details>

<br/>

Wait till it's done, get the output
===

<details><summary>show</summary>
<p>

```bash
kubectl get jobs -w 
kubectl get pods 
kubectl logs pi***
kubectl delete job pi
```

</p>
</details>

<br/>

Create a job with the image busybox that executes the command 'echo hello;sleep 30;echo world'
===

<details><summary>show</summary>
<p>

```bash
kubectl create job busybox --image=busybox -- bin/sh -c 'echo hello;sleep 30;echo world' 
```

</p>
</details>

<br/>

Follow the logs for the pod (you'll wait for 30 seconds)
===

<details><summary>show</summary>
<p>

```bash
kubectl logs busybox-**** -f
```

</p>
</details>

<br/>

See the status of the job, describe it and see the logs
===

<details><summary>show</summary>
<p>

```bash
kubectl get jobs busybox-**** 
kubectl logs busybox
```

</p>
</details>

<br/>

Delete the job
===

<details><summary>show</summary>
<p>

```bash
kubectl delete job busybox-****
```

</p>
</details>

<br/>

Create a job but ensure that it will be automatically terminated by kubernetes if it takes more than 10 seconds to execute
===

<details><summary>show</summary>
<p>

```bash
kubectl create job busybox --image=busybox --dry-run=client -o yaml -- bin/sh -c 'while true; do echo hello; sleep 10;done'  > job.yml

# edit the file created earlier and add activeDeadlineSeconds: 10

#apiVersion: batch/v1
#kind: Job
#metadata:
#  creationTimestamp: null
#  name: busybox
#spec:
#  activeDeadlineSeconds: 10
#  template:
#    metadata:
#      creationTimestamp: null
#    spec:
#      containers:
#      - command:
#        - bin/sh
#        - -c
#        - while true; do echo hello; sleep 10;done
#        image: busybox
#        name: busybox
#        resources: {}
#      restartPolicy: Never
#status: {}
```

</p>
</details>

<br/>

Create the same job, make it run 5 times, one after the other. Verify its status and delete it
===

<details><summary>show</summary>
<p>

```bash
kubectl create job busybox --image=busybox --dry-run=client -o yaml -- bin/sh -c 'echo hello;sleep 5;echo world'  > job.yml

# edit the file created earlier and add completions: 5

#apiVersion: batch/v1
#kind: Job
#metadata:
#  creationTimestamp: null
#  name: busybox
#spec:
#  completions: 5
#  template:
#    metadata:
#      creationTimestamp: null
#    spec:
#      containers:
#      - command:
#        - bin/sh
#        - -c
#        - while true; do echo hello; sleep 10;done
#        image: busybox
#        name: busybox
#        resources: {}
#      restartPolicy: Never
#status: {}
```

</p>
</details>

<br/>

Create the same job, but make it run 5 parallel times
===

<details><summary>show</summary>
<p>

```bash
kubectl create job busybox --image=busybox --dry-run=client -o yaml -- bin/sh -c 'echo hello;sleep 5;echo world'  > job.yml

# edit the file created earlier and add parallelism: 5

#apiVersion: batch/v1
#kind: Job
#metadata:
#  creationTimestamp: null
#  name: busybox
#spec:
#  parallelism: 5
#  template:
#    metadata:
#      creationTimestamp: null
#    spec:
#      containers:
#      - command:
#        - bin/sh
#        - -c
#        - while true; do echo hello; sleep 10;done
#        image: busybox
#        name: busybox
#        resources: {}
#      restartPolicy: Never
#status: {}
```

</p>
</details>

<br/>
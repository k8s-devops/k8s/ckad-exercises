Create a deployment with image nginx:1.7.8, called nginx, having 2 replicas,
defining port 80 as the port that this container exposes (don't create a service for this deployment)
===

<details><summary>show</summary>
<p>

```bash
kubectl create deployment nginx --image=nginx:1.7.8 --replicas=2 --port=80
```

</p>
</details>

<br/>

View the YAML of this deployment
===

<details><summary>show</summary>
<p>

```bash
kubectl get deploy nginx -o yaml
```

</p>
</details>

<br/>

View the YAML of the replica set that was created by this deployment
===

<details><summary>show</summary>
<p>

```bash
kubectl get rs nginx-8f5cc57cf -o yaml
```

</p>
</details>

<br/>

Get the YAML for one of the pods
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods nginx-8f5cc57cf-4zvsg -o yaml
```

</p>
</details>

<br/>

Check how the deployment rollout is going
===

<details><summary>show</summary>
<p>

```bash
kubectl rollout status deployment nginx
```

</p>
</details>

<br/>

Update the nginx image to nginx:1.7.9
===

<details><summary>show</summary>
<p>

```bash
kubectl set image deploy/nginx nginx=nginx:1.7.9 
```

</p>
</details>

<br/>

kubectl the rollout history and confirm that the replicas are OK
===

<details><summary>show</summary>
<p>

```bash
kubectl history deployment nginx

kubectl get rs

kubectl get pods
```

</p>
</details>

<br/>

Undo the latest rollout and verify that new pods have the old image (nginx:1.7.8)
===

<details><summary>show</summary>
<p>

```bash

# get the latest revisions
kubectl rollout history deployment nginx

# display the detail of each revision
kubectl rollout history deployment nginx --revision REVISION_NUMBER

# apply the rollout
kubectl rollout undo deployment nginx --to-revision 1

```

</p>
</details>

<br/>

Do an on purpose update of the deployment with a wrong image nginx:1.91
===

<details><summary>show</summary>
<p>

```bash
kubectl set image deploy/nginx nginx=nginx:1.92
```

</p>
</details>

<br/>

Verify that something's wrong with the rollout
===

<details><summary>show</summary>
<p>

```bash
kubectl rollout status deploy nginx
```

</p>
</details>

<br/>

Return the deployment to the second revision (number 2) and verify the image is nginx:1.7.9
===

<details><summary>show</summary>
<p>

```bash
kubectl rollout undo deployment nginx --to-revision 2
```

</p>
</details>

<br/>

Check the details of the fourth revision (number 4)
===

<details><summary>show</summary>
<p>

```bash
kubectl rollout history deployment nginx --revision 4
```

</p>
</details>

<br/>

Scale the deployment to 5 replicas
===

<details><summary>show</summary>
<p>

```bash
kubectl scale deployment nginx --replicas=5
```

</p>
</details>

<br/>

Autoscale the deployment, pods between 5 and 10, targetting CPU utilization at 80%
===

<details><summary>show</summary>
<p>

```bash
kubectl autoscale deployment nginx --min=5 --max=10 --cpu-percent=80
```

</p>
</details>

<br/>

Pause the rollout of the deployment
===

<details><summary>show</summary>
<p>

```bash
kubectl rollout pause deployment nginx
```

</p>
</details>

<br/>

Update the image to nginx:1.9.1 and check that there's nothing going on, since we paused the rollout
===

<details><summary>show</summary>
<p>

```bash
kubectl rollout undo deployment nginx --to-revision 8

# error: you cannot rollback a paused deployment; resume it first with 'kubectl rollout resume deployment/nginx' and try again

kubectl rollout history deployment  nginx
```

</p>
</details>

<br/>

Delete the deployment and the horizontal pod autoscaler you created
===

<details><summary>show</summary>
<p>

```bash
kubectl delete deployments nginx 

kubectl  delete hpa nginx # horizontalpodautoscaler.autoscaling
```

</p>
</details>

<br/>

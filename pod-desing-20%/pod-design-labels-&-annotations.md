Create 3 pods with names nginx1,nginx2,nginx3. All of them should have the label app=v1
===

<details><summary>show</summary>
<p>

```bash
kubect run nginx1 --image=nginx --restart Never --labels="app=v1"
kubect run nginx2 --image=nginx --restart Never --labels="app=v1"
kubect run nginx3 --image=nginx --restart Never --labels="app=v1"
```

</p>
</details>

<br/>

Show all labels of the pods
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods --show-labels
```
</p>
</details>

<br/>

Change the labels of pod 'nginx2' to be app=v2
===

<details><summary>show</summary>
<p>

```bash
kubectl label pods nginx2 app=v2 --overwrite

# OR

kubectl edit pod nginx2 
# replace v1 with v2
# save and exit the editor mode

```
</p>
</details>

<br/>

Get the label 'app' for the pods (show a column with APP labels)
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods -Lapp 

kubectl get pods --label-columns=app
```
</p>
</details>

<br/>

Get only the 'app=v2' pods
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods -l app=v2

kubectl get pods --selector=app=v2

kubectl get pods -l 'app in (v2)'
```
</p>
</details>

<br/>

Remove the 'app' label from the pods we created before
===

<details><summary>show</summary>
<p>

```bash
kubectl label pods nginx1 nginx2 nginx3 app-

kubectl label po nginx{1..3} app-

kubectl label po -l app app-
```

</p>
</details>

<br/>

Create a pod that will be deployed to a Node that has the label 'accelerator=nvidia-tesla-p100'
===

<details><summary>show</summary>
<p>

```bash
# solution 1 : use NodeSelector
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
  nodeSelector:
    accelerator: nvidia-tesla-p100

# solution 2 : use NoodAffinity
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: accelerator
            operator: In
            values:
            - nvidia-tesla-p100
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
```

</p>
</details>

<br/>

Annotate pods nginx1, nginx2, nginx3 with "description='my description'" value
===

<details><summary>show</summary>
<p>

```bash
kubectl annotate pods nginx{1..3} description='my description'
```

</p>
</details>

<br/>

Check the annotations for pod nginx1
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods nginx1 -o jsonpath='{.metadata.annotations}{"\n"}'

# OR

kubectl describe po nginx1 | grep -i 'annotations'

# OR

kubectl get pods nginx1 -o custom-columns=Name:metadata.name,ANNOTATIONS:metadata.annotations.description
```

</p>
</details>

<br/>

Remove the annotations for these three pods
===

<details><summary>show</summary>
<p>

```bash
kubectl annotate pods nginx{1..3} description-
```

</p>
</details>

<br/>

Remove these pods to have a clean state in your cluster
===

<details><summary>show</summary>
<p>

```bash
kubectl delete pods nginx{1..3}
```

</p>
</details>

<br/>

Create a cron job with image busybox that runs on a schedule of "*/1 * * * *" and writes 'date; echo Hello from the Kubernetes cluster' to standard output
===

<details><summary>show</summary>
<p>

```bash
kubectl create cronjob busybox --image=busybox --schedule="*/1 * * * *" -- /bin/sh -c 'date; echo "Hello from the kubernetets cluster"'
```

</p>
</details>

<br/>

See its logs and delete it
===

<details><summary>show</summary>
<p>

```bash
kubectl get cj
kubectl get jobs --watch
# observe that the pods have a label that mentions their 'parent' job
kubectl get po --show-labels 
kubectl logs busybox-1529745840-m867r
# Bear in mind that Kubernetes will run a new job/pod for each new cron job
kubectl delete cj busybox
```

</p>
</details>

<br/>

Create a cron job with image busybox that runs every minute and writes 'date; echo Hello from the Kubernetes cluster' to standard output. The cron job should be terminated if it takes more than 17 seconds to start execution after its schedule.
===

<details><summary>show</summary>
<p>

```bash
kubectl create cronjob busybox --image=busybox --restart=Never --dry-run=client --schedule="*/1 * * * *" -- /bin/sh -c 'date; echo "Hello from the kubernetets cluster"' > cronjob.yml

# open the yaml file and add this property 'startingDeadlineSeconds = 17'

#apiVersion: batch/v1beta1
#kind: CronJob
#metadata:
#  name: busybox
#spec:
#  startingDeadlineSeconds = 17
#  jobTemplate:
#    metadata:
#      name: busybox
#    spec:
#      template:
#        metadata:
#        spec:
#          containers:
#          - command:
#            - /bin/sh
#            - -c
#            - date; echo "Hello from the kubernetets cluster"
#            image: busybox
#            name: busybox
#            resources: {}
#          restartPolicy: Never
#  schedule: '*/1 * * * *'

```
</p>
</details>

<br/>
Create busybox pod with two containers, each one will have the image busybox and will run the 'sleep 3600' command. Make both containers mount an emptyDir at '/etc/foo'. Connect to the second busybox, write the first column of '/etc/passwd' file to '/etc/foo/passwd'. Connect to the first busybox and write '/etc/foo/passwd' file to standard output. Delete pod.
===

<details><summary>show</summary>
<p>

```bash
# create busybox.yml
kubectl run busybox --image=busybox --restart Never --dry-run=client -o yaml -- bin/sh -c 'sleep 3600' > busybox.ym
# edit busybox.ylml and add EmptyDir volume and mount it
```

```yaml
    apiVersion: v1
kind: Pod
metadata:
  labels:
    run: busybox
  name: busybox
spec:
  containers:
  - image: busybox
    name: busybox
    args:
    - bin/sh
    - -c
    - sleep 3600
    volumeMounts:
    - mountPath: /etc/foo
      name: my-volume
    resources: {}
  - image: busybox
    name: busybox
    args:
    - bin/sh
    - -c
    - sleep 3600
    volumeMounts:
    - mountPath: /etc/foo
      name: my-volume
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
  volumes:
  - name: my-volume
    emptyDir: {}
status: {}
```

```bash
# connect to the second container
kubectl exec -it busybox -c busybox2 -- bin/sh
cat /etc/password | cut -f 1 -d ':' > /etc/foo/passwd
cat /etc/foo/passwd
exit

# connect to the first container
kubectl exec -it busybox -c busybox1 -- bin/sh -c 'cat /etc/foo/passwd'

# delete the pod
kubectl delete pods busybox
```

</p>
</details>

<br/>

Create a PersistentVolume of 10Gi, called 'myvolume'. Make it have accessMode of 'ReadWriteOnce' and 'ReadWriteMany', storageClassName 'normal', mounted on hostPath '/etc/foo'. Save it on pv.yaml, add it to the cluster. Show the PersistentVolumes that exist on the cluster
===

<details><summary>show</summary>
<p>

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: myvolume
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
    - ReadWriteMany
  storageClassName: normal
  hostPath:
    path: /etc/foo
```

```bash
kubectl create -f pv.yml --save-config --record
kubectl get pv
```

</p>
</details>

<br/>

Create a PersistentVolumeClaim for this storage class, called mypvc, a request of 4Gi and an accessMode of ReadWriteOnce, with the storageClassName of normal, and save it on pvc.yaml. Create it on the cluster. Show the PersistentVolumeClaims of the cluster. Show the PersistentVolumes of the cluster
===

<details><summary>show</summary>
<p>

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mypvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 4Gi
  storageClassName: normal
```

```bash
kubectl get pvc # will show as 'Bound'
kubectl get pv # will show as 'Bound' as well
```
</p>
</details>

<br/>

Create a busybox pod with command 'sleep 3600', save it on pod.yaml. Mount the PersistentVolumeClaim to '/etc/foo'. Connect to the 'busybox' pod, and copy the '/etc/passwd' file to '/etc/foo/passwd'
===
<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --restart Never --dry-run=client -o yaml -- bin/sh -c 'sleep 3600' > pod.yml

# edit the pod.yml to mount the volume
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: busybox
  name: busybox
spec:
  containers:
  - image: busybox
    name: busybox
    command:
    - bin/sh
    - -c
    - sleep 3600
    volumeMounts:
      - name: myvolume
        mountPath: /etc/foo
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
  volumes:
  - name: myvolume
    persistentVolumeClaim:
      claimName: mypvc
status: {}
```

```bash
kubectl create -f pod.yml --save-config --record

# copy the /etc/passwd
kubectl exec -it busybox -- cp /etc/passwd /etc/foo/passwd
```

</p>
</details>

<br/>

Create a second pod which is identical with the one you just created (you can easily do it by changing the 'name' property on pod.yaml). Connect to it and verify that '/etc/foo' contains the 'passwd' file. Delete pods to cleanup. Note: If you can't see the file from the second pod, can you figure out why? What would you do to fix that?
===
<details><summary>show</summary>
<p>

```yaml
# pod2.yml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: busybox2
  name: busybox2
spec:
  containers:
  - image: busybox2
    name: busybox
    command:
    - bin/sh
    - -c
    - sleep 3600
    volumeMounts:
      - name: myvolume
        mountPath: /etc/foo
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Never
  volumes:
  - name: myvolume
    persistentVolumeClaim:
      claimName: mypvc
status: {}
```

```bash
kubectl create -f pod2.yml --save-config --record

# connect to the second pod and verify the content of /etc/passwd
kubectl exec -it busybox -- cp /etc/passwd /etc/foo/passwd
```

</p>
</details>

<br/>

Create a busybox pod with 'sleep 3600' as arguments. Copy '/etc/passwd' from the pod to your local folder
===
<details><summary>show</summary>
<p>

```bash
kubectl run busybox3 --image=busybox --restart Never --command -- bin/sh -c 'sleep 3600'

kubectl cp busybox:/etc/passwd /tmp/passwd
```

</p>
</details>

<br/>

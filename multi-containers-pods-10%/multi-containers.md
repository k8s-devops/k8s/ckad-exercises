Create a Pod with two containers, both with image busybox and command "echo hello; sleep 3600".
Connect to the second container and run 'ls'
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox  --restart=Never --dry-run=client -o yaml -- bin/sh -c 'echo Hello world; sleep 3600' > pod.yml

# edit the earlier created file pod.yam and add the second container like that : 

# containers:
#  - args:
#    - bin/sh
#    - -c
#    - echo Hello world; sleep 3600
#    image: busybox
#    name: busybox1
#    resources: {}
#  - args:
#    - bin/sh
#    - -c
#    - echo Hello world; sleep 3600
#    image: busybox
#    name: busybox2
#    resources: {}

# execute the 'ls' in the second container
kubectl exec -it busybox -c busybox2 -- ls

kubectl exec -it busybox -c busybox2 -- bin/sh
ls
```

</p>
</details>

<br/>

Create pod with nginx container exposed at port 80. Add a busybox init container which downloads a page using "wget -O /work-dir/index.html http://neverssl.com/online". Make a volume of type emptyDir and mount it in both containers. For the nginx container, mount it on "/usr/share/nginx/html" and for the initcontainer, mount it on "/work-dir". When done, get the IP of the created pod and create a busybox pod and run "wget -O- IP"
===

<details><summary>show</summary>
<p>

```bash
# create nginx pods and save the result in a file called pod.yml
kubectl run nginx --image=nginx --port 80 --restart Never --dry-run=client -o yaml > pod.yml

# cat pod.yml
# the final pod.yml looks like

#apiVersion: v1
#kind: Pod
#metadata:
#  labels:
#    run: nginx
#  name: nginx
#spec:
#  initContainers:
#  - name: busybox
#    image: busybox
#    command: ['sh', '-c', "wget -O /work-dir/index.html http://neverssl.com/online"]
#    volumeMounts:
#    - mountPath: /work-dir
#      name: common-volume
#  containers:
#  - image: nginx
#    name: nginx
#    ports:
#    - containerPort: 80
#    resources: {}
#    volumeMounts:
#    - mountPath: //usr/share/nginx/html
#      name: common-volume
#  restartPolicy: Never
#  volumes:
#  - name: common-volume
#    emptyDir: {}

# apply changes
kubectl apply -f pod.yml

# get pods IP
export POD_IP=kubectl get pods nginx -o jsonpath='{.status.podIP}{"\n"}'

# create the last busybox pod 
kubectl run busy --image=busybox --restart Never -- bin/sh -c "wget -O- $POD_IP"

# you can expose a LoadBalancer service
kubectl expose pod nginx --port=9000 --target-port=80 --type LoadBalancer --name=frontend
```

</p>
</details>

<br/>


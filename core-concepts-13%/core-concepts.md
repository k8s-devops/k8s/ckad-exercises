Create a namespace called 'my_ns' and a pod with image nginx called nginx on this namespace
===

<details><summary>show</summary>
<p>

```bash
kubectl create ns my_ns
kubectl run nginx --image=nginx --restart=Never -n my_ns
```

</p>
</details>

<br/>

Create the same nginx pod using Yaml in the namespace my_ns
===

<details><summary>show</summary>
<p>

```bash
kubectl run nginx --image=nginx --restart=Never -n my_ns --dry-run=client -o yaml > nginx_pod.yml
kubectl create -f nginx_pod.yml --save-config --record
```

</p>
</details>

<br/>

Create a busybox pod that runs the command 'env' in the namespace my_ns. Check the output
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --restart=Never -n my_ns -- env
kubectl logs -n my_ns busybox
```

</p>
</details>

<br/>

Create a busybox pod using yaml that runs the command 'env' in the namespace my_ns. Check the output
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --restart=Never -n my_ns --dry-run=client  -o yaml -- env > busybox_pod.yml
kubectl create -f busybox_pod.yml --save-config --record
```

</p>
</details>

<br/>

Generate a Yaml file for a new namespace called 'ns_2' without creating it 
===

<details><summary>show</summary>
<p>

```bash
kubectl create ns ns_2 --dry-run=client -o yaml > ns_2.yml
```

</p>
</details>

<br/>

Get the YAML for a new ResourceQuota called 'my_resource_quota' with hard limits of 1 CPU, 1G memory and 2 pods without creating it 
===

<details><summary>show</summary>
<p>

```bash
kubectl create quota my_resource_quota --hard=cpu=1,memory=1G,pods=2 --dry-run=client -o yaml
```

</p>
</details>

<br/>

Display pods on all namespaces 
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods --all-namespaces
```

</p>
</details>

<br/>

Create a pod with image nginx called nginx and expose traffic on port 80
===

<details><summary>show</summary>
<p>

```bash
kubectl run nginx --image=nginx --dry-run=client -o yaml --port=80
```

</p>
</details>

<br/>

Change pod's image to nginx:1.7.1. Observe that the container will be restarted as soon as the image gets pulled
===

<details><summary>show</summary>
<p>

```bash
kubectl run nginx --image=nginx  --port=80
kubectl set image pod/nginx nginx=nginx:1.17.1 
kubectl describe pods nginx
kubectl get pods nginx -w 
kubectl get pods nginx -o jsonpath='{.spec.containers[].image}{"\n"}'
```

</p>
</details>

<br/>

Get nginx pod's ip created in previous step, use a temp busybox image to wget its '/'
===

<details><summary>show</summary>
<p>

```bash
# run the following command to pick the IP address 'podIP field'
kubectl get pods nginx -o yaml 
# run new busybox container and execute wger directly on it 
kubectl run busybox --image=busybox --rm --restart=Never -- wget 172.17.0.3:80

# Alternative solution
POD_IP = $(kubectl get pods nginx -o jsonpath='{.status.podIP}{"\n"}')
POD_PORT=$(kubectl get pods nginx -o jsonpath='{.spec.containers[].ports[].containerPort}{"\n"}')
kubectl run busybox --image=busybox --rm -it --restart=Never -- wget $POD_IP:$POD_PORT
```

</p>
</details>

<br/>

Get nginx pod's YAML
===

<details><summary>show</summary>
<p>

```bash
kubectl get pods -o yaml
kubectl get pods -oyaml
kubectl get pods --output=yaml
kubectl get pods --output yaml
```

</p>
</details>

<br/>

Get information about the pod, including details about potential issues (e.g. pod hasn't started)
===

<details><summary>show</summary>
<p>

```bash
kubectl describe pods nginx
```

</p>
</details>

<br/>

Get nginx pod's logs
===

<details><summary>show</summary>
<p>

```bash
kubectl logs nginx
```

</p>
</details>

<br/>

Get logs about the previous ngix instance
===

<details><summary>show</summary>
<p>

```bash
kubectl logs nginx --previous
kubectl logs nginx -p
```

</p>
</details>

<br/>

Execute a simple shell on the nginx pod
===

<details><summary>show</summary>
<p>

```bash
kubeclt exec nginx -it -- echo 'hello'
kubeclt exec nginx -it -- bin/bash
```

</p>
</details>

<br/>

Create a busybox pod that echoes 'Hello world' and then exits
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox -it --restart=Never -- echo 'Hello world'
```

</p>
</details>

<br/>

Do the same, but have the pod deleted automatically when it's completed
===

<details><summary>show</summary>
<p>

```bash
kubectl run busybox --image=busybox --rm -it --restart=Never -- echo 'Hello world'
kubectl run busybox --image=busybox --rm -it --restart=Never -- /bin/sh -c 'echo Hello world'
```

</p>
</details>

<br/>

Create an nginx pod and set an env value as 'var1=val1'. Check the env value existence within the pod
===

<details><summary>show</summary>
<p>

```bash
# create the pod
kubectl run nginx --image=nginx --env="var1=val1"
# check envs
kubectl exec nginx -it -- env
kubectl exec nginx -it -- sh -c 'echo $var1'
kubectl describe pods nginx | grep var1
```

</p>
</details>

<br/>